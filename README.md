linux-conf
==========

Common configurations on my linux machines.

.bashrc contains all the bashrc shortcuts
inputrc adds completion-ignore-case to /etc/inputrc
.vimrc.local is the local definitions for use with spf-13 vim plugin suite
personalSettings.sh adds shortcuts for dual screen resolution fixes

